<!-- resources/views/users/create.blade.php -->

@extends('layout.main')
@section('menu-title', 'User Create')
@section('menu-route-title', 'User Create')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Create User</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('users.store') }}" method="POST" id="userForm">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                    id="name" name="name" value="{{ old('name') }}" required>
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror"
                                    id="email" name="email" value="{{ old('email') }}" required>
                                @error('email')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        id="password" name="password" required>
                                    <button type="button" class="btn btn-outline-secondary" id="togglePassword">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                </div>
                                @error('password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const passwordInput = document.getElementById("password");
            const togglePasswordButton = document.getElementById("togglePassword");

            togglePasswordButton.addEventListener("click", function() {
                const type = passwordInput.getAttribute("type") === "password" ? "text" : "password";
                passwordInput.setAttribute("type", type);

                const eyeIcon = this.querySelector("i");
                if (eyeIcon.classList.contains("fa-eye")) {
                    eyeIcon.classList.remove("fa-eye");
                    eyeIcon.classList.add("fa-eye-slash");
                } else {
                    eyeIcon.classList.remove("fa-eye-slash");
                    eyeIcon.classList.add("fa-eye");
                }
            });

            document.getElementById("userForm").addEventListener("submit", function(e) {
                const name = document.getElementById("name").value;
                const email = document.getElementById("email").value;
                const password = passwordInput.value;

                if (!/^[a-zA-Z\s]{5,24}$/.test(name)) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Invalid Input',
                        text: 'Invalid name format. Only alphabetic characters and spaces allowed, up to 24.'
                    });
                    e.preventDefault();
                    return false;
                }

                if (!/\S+@\S+\.\S+/.test(email)) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Invalid Input',
                        text: 'Invalid email format.'
                    });
                    e.preventDefault();
                    return false;
                }

                if (password.length < 8) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Invalid Input',
                        text: 'Password must be at least 8 characters long.'
                    });
                    e.preventDefault();
                    return false;
                }
            });
        });
    </script>
@endsection
