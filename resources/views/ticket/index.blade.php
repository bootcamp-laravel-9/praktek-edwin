@extends('layout.main')

@section('menu-ticket', 'active')
@section('menu-title', 'Report')
@section('menu-route-title', 'Report')
@section('content')
    <div class="container py-4">
        <div class="card shadow-sm">
            <div class="card-body">
                <div class="mb-3 row">
                    <div class="col-md-3">
                        <label for="dateAfter">Date After:</label>
                        <input type="date" id="dateAfter" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label for="dateBefore">Date Before:</label>
                        <input type="date" id="dateBefore" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label for="categoryFilter">Category:</label>
                        <select id="categoryFilter" class="form-control">
                            <option value="">All Category</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category }}">{{ $category }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Search:</label>
                        <input type="text" class="form-control" placeholder="Type to search..." id="customSearchBox">
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="ticketsTable">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th>#</th>
                                <th>Ticket No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Date</th>
                                <th>Category</th>
                                <th>Total Ticket</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $rowNumber = 1; @endphp
                            @foreach ($tickets as $ticket)
                                <tr>
                                    <td>{{ $rowNumber++ }}</td>
                                    <td>{{ $ticket['no_ticket'] }}</td>
                                    <td>{{ $ticket['nama'] }}</td>
                                    <td>{{ $ticket['email'] }}</td>
                                    <td>{{ $ticket['no_telp'] }}</td>
                                    <td>{{ $ticket['address'] }}</td>
                                    <td>{{ $ticket['date_ticket'] }}</td>
                                    <td>
                                        @foreach ($ticket['details'] as $detail)
                                            {{ $detail['category']['name'] }}
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach ($ticket['details'] as $detail)
                                            {{ $detail['total_ticket'] }}
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

    <script>
        $(document).ready(function() {
            var table = $('#ticketsTable').DataTable({
                "pagingType": "simple_numbers",
                "language": {
                    "lengthMenu": "",
                    "search": "",
                },

                "dom": "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 d-flex justify-content-end'p>>",
            });

            $('#customSearchBox').keyup(function() {
                table.search($(this).val()).draw();
            });

            $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
                var minDateStr = $('#dateAfter').val();
                var maxDateStr = $('#dateBefore').val();
                var rowDateStr = data[6];

                if (!minDateStr && !maxDateStr) {
                    return true;
                }

                var rowDate = moment(rowDateStr, "YYYY-MM-DD");
                var minDate = minDateStr ? moment(minDateStr, "YYYY-MM-DD") : null;
                var maxDate = maxDateStr ? moment(maxDateStr, "YYYY-MM-DD") : null;

                if (minDate && maxDate) {
                    return rowDate.isSameOrAfter(minDate) && rowDate.isSameOrBefore(maxDate);
                } else if (minDate) {
                    return rowDate.isSameOrAfter(minDate);
                } else if (maxDate) {
                    return rowDate.isSameOrBefore(maxDate);
                }
            });


            $('#dateAfter, #dateBefore, #categoryFilter').on('change', function() {
                table.draw();
            });

            $('#categoryFilter').on('change', function() {
                var search = $.fn.dataTable.util.escapeRegex($(this).val());
                table.column(7).search(this.value ? '^' + search + '$' : '', true, false).draw();
            });
        });
    </script>

@endsection
