@extends('layout.main')

@section('menu-ticket', 'active')
@section('menu-title', 'Create Ticket')
@section('menu-route-title', 'Create Ticket')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <form action="{{ url('/addTickets') }}" method="POST" id="ticketForm">
                            @csrf
                            <div class="mb-3">
                                <label for="nama" class="form-label">Name</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                    id="nama" name="nama" value="{{ old('nama') }}" required>
                                @error('nama')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror"
                                    id="email" name="email" value="{{ old('email') }}">
                                @error('email')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="no_telp" class="form-label">Phone</label>
                                <input type="text" class="form-control @error('no_telp') is-invalid @enderror"
                                    id="no_telp" name="no_telp" value="{{ old('no_telp') }}">
                                @error('no_telp')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="address" class="form-label">Address</label>
                                <input type="text" class="form-control @error('address') is-invalid @enderror"
                                    id="address" name="address" value="{{ old('address') }}" required>
                                @error('address')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="date_ticket" class="form-label">Date</label>
                                <input type="date" class="form-control @error('date_ticket') is-invalid @enderror"
                                    id="date_ticket" name="date_ticket" value="{{ old('date_ticket') }}"
                                    min="{{ date('Y-m-d') }}" required>
                                @error('date_ticket')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group
                                    mb-3">
                                <label for="ticket_category_id" class="form-label">Category</label>
                                <select id="ticket_category_id" name="details[0][ticket_category_id]" class="form-control"
                                    required onchange="categoryChanged(this);">
                                    @foreach ($categories as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group mb-3">
                                <label for="total_ticket" class="form-label">Total Ticket</label>
                                <input type="number" class="form-control" id="total_ticket" min='1'
                                    name="details[0][total_ticket]" required>
                            </div>

                            <div class="d-grid gap-2">
                                <button type="submit" class="btn btn-primary btn-block">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function categoryChanged(select) {
            if (select.value) {
                select.classList.add('selected');
            } else {
                select.classList.remove('selected');
            }
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const form = document.getElementById('ticketForm');
            const name = document.getElementById('nama');
            const email = document.getElementById('email');
            const phone = document.getElementById('no_telp');
            const address = document.getElementById('address');
            const dateTicket = document.getElementById('date_ticket');

            form.addEventListener('submit', function(e) {
                let errors = [];

                if (name.value.length < 8 || name.value.length > 20) {
                    errors.push('Name must be between 8 and 20 characters.');
                }

                const emailRegex = /^\S+@\S+\.\S+$/;
                if (!emailRegex.test(email.value)) {
                    errors.push('Invalid email format.');
                }

                const phoneRegex = /^\d{8,15}$/;
                if (!phoneRegex.test(phone.value)) {
                    errors.push('Phone number must be between 8 and 15 digits.');
                }

                if (address.value.length > 255) {
                    errors.push('Address must not exceed 255 characters.');
                }

                const today = new Date().toISOString().split('T')[0];
                if (dateTicket.value < today) {
                    errors.push('Date cannot be in the past.');
                }

                if (errors.length > 0) {
                    e.preventDefault();
                    Swal.fire({
                        icon: 'error',
                        title: 'Validation Error',
                        html: errors.join('<br>'),
                    });
                }
            });
        });
    </script>

@endsection
