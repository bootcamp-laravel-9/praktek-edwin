<footer class="main-footer">
    <strong>Copyright &copy; 2024
        <a href="https://adminlte.io">Edwin$Co</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> Stable
    </div>
</footer>
