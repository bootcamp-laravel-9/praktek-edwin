<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Simple Login</title>
    <!-- Bootstrap 5 CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <style>
        body {
            background-color: #f0f2f5;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
            color: #1d1d1f;
        }

        .login-card {
            width: 500px;
            margin: 5% auto;
            padding: 2rem;
            border-radius: 15px;
        }

        .btn-primary {
            background-color: #007bff;
            border: none;
            border-radius: 5px;
            padding: 8px;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
        }

        .fas.fa-eye,
        .fas.fa-eye-slash {
            color: #d1d6db;
        }

        .input-group-text {
            background-color: #1f1a41;
            border-left: 0;
        }

        .btn-primary:hover {
            background-color: #0056b3;
        }

        .btn-neutral {
            background-color: #f2f2f7;
            border: none;
            border-radius: 20px;
            padding: 10px 15px;
        }

        .btn-neutral:hover {
            background-color: #e0e0e0;
        }

        .form-control {
            border-radius: 5px;
            border: 1px solid #ced4da;
        }

        .bg-primary {
            background-color: #007bff !important;
        }

        .text-white {
            color: #fff !important;
        }

        .text-warning {
            color: #3407ff !important;
        }

        .font-semibold {
            font-weight: 600;
        }

        .display-6 {
            font-size: 1.5rem;
            font-weight: 300;
            line-height: 1.2;
        }

        /* Circle shape */
        .rounded-circle {
            position: absolute;
            bottom: -50px;
            right: -50px;
            width: 200px;
            height: 200px;
            border-radius: 50%;
            background-color: #ff6f00;
        }

        .overflow-hidden {
            overflow: hidden;
        }

        @media (min-width: 992px) {
            .position-fixed {
                position: fixed;
            }

            .start-0 {
                left: 0;
            }

            .top-0 {
                top: 0;
            }

            .overflow-y-hidden {
                overflow-y: hidden;
            }

            .h-screen {
                height: 100vh;
            }

            .border-left-lg {
                border-left: 1px solid #ececec;
            }

            .min-h-lg-screen {
                min-height: 100vh;
            }
        }
    </style>
</head>

<body class="bg-light overflow-hidden">
    <div class="d-flex justify-content-center">
        <div
            class="col-lg-5 col-xl-4 px-5 py-5 bg-primary text-white position-fixed start-0 top-0 h-screen d-none d-lg-flex flex-column justify-content-center">
            <h1 class="font-weight-bold mb-4">
                Welcome Back!
            </h1>
            <p class="fs-4">
                Login first to explore this website...
            </p>
            <div class="rounded-circle"></div>
        </div>
    </div>
    <div class="col-12 col-md-9 col-lg-7 offset-lg-5 min-h-lg-screen d-flex align-items-center py-lg-5 px-lg-5">
        <div class="login-card">
            <h1 class="text-left mb-4">Login</h1>

            <form action="{{ route('loginAttempt') }}" method="POST">
                @csrf
                <div class="mb-4">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="email" name="email"
                        placeholder="name@example.com" required>
                </div>
                <div class="mb-4">
                    <label for="password" class="form-label">Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control" id="password" name="password"
                            placeholder="Password" required>
                        <button type="button" class="btn input-group-text" onclick="togglePasswordVisibility()">
                            <i class="fas fa-eye" id="togglePasswordIcon"></i>
                        </button>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary w-100">Sign in</button>
            </form>
        </div>
    </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Include SweetAlert2 JS -->
    <!-- Include SweetAlert2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function togglePasswordVisibility() {
            let passwordInput = document.getElementById('password');
            let toggleIcon = document.getElementById('togglePasswordIcon');
            if (passwordInput.type === "password") {
                passwordInput.type = "text";
                toggleIcon.classList.remove('fa-eye');
                toggleIcon.classList.add('fa-eye-slash');
            } else {
                passwordInput.type = "password";
                toggleIcon.classList.remove('fa-eye-slash');
                toggleIcon.classList.add('fa-eye');
            }
        }
    </script>
    <script>
        @if (session('success'))
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: "{{ session('success') }}",
                timer: 3000,
                timerProgressBar: true
            });
        @endif
        @if (session('error'))
            Swal.fire({
                icon: 'error',
                title: 'Login Failed',
                text: "{{ session('error') }}",
                timer: 5000,
                timerProgressBar: true
            });
        @endif
        @if (session('message'))
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: "Coba Lagi",
                timer: 5000,
                timerProgressBar: true
            });
        @endif
    </script>
</body>

</html>
