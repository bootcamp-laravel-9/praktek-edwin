@extends('layout.main')

@section('title', 'Dashboard')

@section('content')
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Discover Adventures Anew</title>
        <!-- Bootstrap CSS, ensure you have this in your project -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/5.1.0/css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>
        <main class="container py-5">
            <!-- Jumbotron/Introduction -->
            <div class="p-5 mb-4 bg-light rounded-3 border">
                <div class="container-fluid py-5">
                    <h1 class="display-5 fw-bold">Welcome to Flight & Tour Co.!</h1>
                    <p class="col-md-8 fs-4">Embark on your next adventure with our exclusive flight and touring packages.
                        Explore the world with comfort and ease, all at unbeatable prices.</p>
                    <button class="btn btn-primary btn-lg" type="button">Discover More</button>
                </div>
            </div>

            <!-- Promotion Cards -->
            <div class="row row-cols-1 row-cols-md-3 g-4">
                <div class="col">
                    <div class="card h-100">
                        <div class="card-body">
                            <h5 class="card-title">Family Escapades</h5>
                            <p class="card-text">Enjoy unforgettable moments with family-friendly adventures. Explore
                                destinations with activities tailored for all ages.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Starting at $999</small>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-body">
                            <h5 class="card-title">Romantic Retreats</h5>
                            <p class="card-text">Discover romantic escapes that promise to ignite passion. Luxurious,
                                intimate, and absolutely unforgettable.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Starting at $1,299</small>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-body">
                            <h5 class="card-title">Adventure Thrills</h5>
                            <p class="card-text">For the thrill-seekers and adventurers, explore our packages that are sure
                                to get your heart racing.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Starting at $899</small>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <!-- Bootstrap Bundle with Popper.js -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.1.0/js/bootstrap.bundle.min.js"></script>
    </body>

    </html>

@endsection
