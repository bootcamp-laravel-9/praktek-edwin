<?php

namespace Database\Seeders;

use App\Models\TicketDetail;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $details = [
            [
                'ticket_header_id' => 1,
                'ticket_category_id' => 13,
                'total_ticket' => 3,
            ],
            [
                'ticket_header_id' => 2,
                'ticket_category_id' => 14,
                'total_ticket' => 1,
            ],
        ];

        DB::beginTransaction();

        foreach ($details as $detail) {
            TicketDetail::firstOrCreate($detail);
        }

        DB::commit();
    }
}