<?php

namespace Database\Seeders;

use App\Models\TicketCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name' => 'First Class', 'detail' => 'Support for premium service for foods and drinks.'],
            ['name' => 'Business Class', 'detail' => 'Support for the lounge facilities.'],
            ['name' => 'Economy Class', 'detail' => 'The price is relatively affordable,.'],
        ];

        DB::beginTransaction();

        foreach ($categories as $category) {
            TicketCategory::firstOrCreate($category);
        }

        DB::commit();
    }
}