<?php

namespace Database\Seeders;

use App\Models\TicketHeader;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketHeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $headers = [
            [
                'no_ticket' => 'TKT-10001',
                'nama' => 'John Doe',
                'email' => 'john.doe@example.com',
                'no_telp' => '1234567890',
                'address' => '123 Main St, Anytown, USA',
                'date_ticket' => '2024-03-02'
            ],
            [
                'no_ticket' => 'TKT-10002',
                'nama' => 'Jane Doe',
                'email' => 'jane.doe@example.com',
                'no_telp' => '0987654321',
                'address' => '456 Elm St, Anothertown, USA',
                'date_ticket' => '2024-03-02'
            ],
        ];

        DB::beginTransaction();

        foreach ($headers as $header) {
            TicketHeader::firstOrCreate($header);
        }

        DB::commit();
    }
}