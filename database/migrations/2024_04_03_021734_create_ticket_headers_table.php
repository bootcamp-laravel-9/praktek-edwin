<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_headers', function (Blueprint $table) {
            $table->id();
            $table->string('no_ticket')->unique();
            $table->string('nama');
            $table->string('email')->nullable();
            $table->integer('no_telp')->nullable();
            $table->string('address');
            $table->date('date_ticket');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_headers');
    }
};