<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthRepository
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param array<string, mixed> $data
     * @return mixed
     */
    public function login($request)
    {
        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            throw new \Exception('Login failed, please check your credentials', 400);
        }

        $user = User::where('email', $request->email)->first();
        $token = $user->createToken(config('app.name'))->plainTextToken;

        return [
            'status' => true,
            'message' => 'Login successful',
            'data' => [
                'user' => $user,
                'token' => $token,
            ],
        ];
    }
}