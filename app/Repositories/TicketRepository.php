<?php

namespace App\Repositories;

use App\Http\Resources\TicketResource;
use App\Models\TicketCategory;
use App\Models\TicketDetail;
use App\Models\TicketHeader;

class TicketRepository
{
    protected $ticketHeader;
    protected $ticketDetail;
    protected $ticketCategory;
    public function
    __construct(TicketHeader $ticketHeader, TicketDetail $ticketDetail, TicketCategory $ticketCategory)
    {
        $this->ticketHeader = $ticketHeader;
        $this->ticketDetail = $ticketDetail;
        $this->ticketCategory = $ticketCategory;
    }

    public function storeOrUpdate($data, $method = 'POST')
    {
        $ticketHeaderData = [
            'no_ticket' => isset($data['id']) ? 'TKT-1000' . $data['id'] : 'TKT-' . uniqid(),
            'nama' => $data['nama'],
            'email' => $data['email'],
            'no_telp' => $data['no_telp'],
            'address' => $data['address'],
            'date_ticket' => $data['date_ticket'],
        ];

        $ticket = $this->ticketHeader->updateOrCreate(
            ['id' => $data['id'] ?? null],
            $ticketHeaderData
        );

        if ($method === 'PUT') {
            $ticket->details()->delete();
        }

        foreach ($data['details'] as $detail) {
            $ticket->details()->create([
                'ticket_category_id' => $detail['ticket_category_id'],
                'total_ticket' => $detail['total_ticket'],
            ]);
        }

        return $ticket->load('details.category');
    }

    public function all($id = null)
    {
        if ($id === null) {
            return TicketHeader::with(['details.category'])->get();
        } else {
            return TicketHeader::with(['details.category'])->find($id);
        }
    }
}