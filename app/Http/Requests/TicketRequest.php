<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // You may set authorization logic here if needed
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
            'no_telp' => 'nullable|string|max:20',
            'address' => 'required|string|max:255',
            'date_ticket' => 'required|date',
            'metode_pembayaran' => 'required|string|max:255',
            'total' => 'required|numeric|min:1',
            'kategori' => 'required|exists:ticket_categories,id',
        ];
    }
}