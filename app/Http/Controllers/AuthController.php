<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Models\User;
use App\Repositories\AuthRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);


        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {

            return response()->json([
                'status' => false,
                'message' => 'Login failed, please check your credentials',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $user = User::where('email', $request->email)->firstOrFail();
        $token = $user->createToken(config('app.name'))->plainTextToken;

        return response()->json([
            'status' => true,
            'message' => 'Login successful',
            'data' => [
                'user' => $user,
                'token' => $token,
            ],
        ], Response::HTTP_OK);
    }

    public function loginAttempt(AuthRequest $request)
    {
        try {
            $hashedPassword = Hash::make($request->password);

            $apiRequest = Request::create(url('api/login'), 'POST', [
                'email' => $request->email,
                'password' => $request->password,
            ]);
            // dd($apiRequest);
            $apiRequest->headers->set('Accept', 'application/json');

            $response = app()->handle($apiRequest);
            $content = json_decode($response->getContent());

            if (isset($content->status) && $content->status === true) {
                $user = User::where('email', $request->email)->first();
                $token = $user->createToken(config('app.name'))->plainTextToken;
                $request->session()->put('LoginSession', $token);

                return redirect()->intended('dashboard')->with('success', 'Login Success');
            } else {
                $errorMessage = $content->message;
                return back()->withInput($request->only('email'))->with('error', $errorMessage);
            }
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }


    public function loginView()
    {
        return view('auth.login');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}