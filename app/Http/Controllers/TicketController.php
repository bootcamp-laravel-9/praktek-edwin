<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketRequest;
use App\Http\Resources\TicketResource;
use App\Repositories\TicketRepository;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    protected $ticketRepository;

    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    public function index(Request $request)
    {
        try {
            $data = $this->ticketRepository->all($request->id);
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Data not found'], 404);
        }
    }

    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|string',
            'email' => 'nullable|email',
            'no_telp' => 'nullable|string',
            'address' => 'required|string',
            'date_ticket' => 'required|date',
            'details' => 'required|array',
            'details.*.total_ticket' => 'required|integer',
            'details.*.ticket_category_id' => 'required|exists:ticket_categories,id',
        ]);

        try {
            $ticket = $this->ticketRepository->storeOrUpdate($validatedData, $request->method());
            return response()->json(['message' => 'Ticket saved successfully', 'data' => new TicketResource($ticket)], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}