<?php

namespace App\Http\Controllers;

use App\Models\TicketCategory;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ConsumeApiController extends Controller
{
    public function index()
    {
        $apiRequest = Request::create(url('api/getTicket'), 'GET');
        $apiResponse = app()->handle($apiRequest);

        if ($apiResponse->getStatusCode() === 200) {
            $tickets = json_decode($apiResponse->getContent(), true);

            $categories = collect($tickets)->flatMap(function ($ticket) {
                return collect($ticket['details'])->pluck('category.name')->unique();
            })->unique()->sort()->values()->all();

            return view('ticket.index', [
                'tickets' => $tickets,
                'categories' => $categories
            ]);
        } else {
            return response()->json(['error' => 'Failed to fetch tickets'], $apiResponse->getStatusCode());
        }
    }

    public function showCreateForm()
    {
        $categories = TicketCategory::pluck('name', 'id');
        return view('ticket.create', compact('categories'));
    }

    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|string|min:8|max:20',
            'email' => 'required|email',
            'no_telp' => 'integer',
            'address' => 'required|string',
            'date_ticket' => 'required|date',
            'details' => 'required',
        ]);


        $requestData = [
            'nama' => $validatedData['nama'],
            'email' => $validatedData['email'],
            'no_telp' => $validatedData['no_telp'],
            'address' => $validatedData['address'],
            'date_ticket' => $validatedData['date_ticket'],
            'details' => $validatedData['details'],
        ];

        $apiRequest = Request::create(url('api/addTicket'), 'POST', $requestData);

        $apiResponse = app()->handle($apiRequest);
        if ($apiResponse->getStatusCode() === 200) {
            $responseArray = json_decode($apiResponse->getContent(), true);
            return redirect()->route('ticket.create')->with('success', 'Ticket created/updated successfully');
        } else {
            $errorResponse = json_decode($apiResponse->getContent(), true);
            return back()->withInput()->with('error', $errorResponse['error'] ?? 'Failed to create/update ticket');
        }
    }
}