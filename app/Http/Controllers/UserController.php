<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    }
    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5|max:24|regex:/^[^0-9]*$/',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
        ]);

        $data = $request->all();
        $data['password'] = Hash::make($request->password);

        User::create($data);

        return redirect()->route('user.index')
            ->with('success', 'User created successfully.');
    }

    public function updateUser($id)
    {
        $user = User::find($id);
        return view('user.update', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'min:5', 'max:24', 'regex:/^[^0-9]*$/'],
            'email' => ['required', 'email', 'max:25'],
            'password' => ['nullable', 'confirmed', 'min:8', 'max:15'],
        ]);

        $user = $user->find($request->id);

        $user->name = $request->name;
        $user->email = $request->email;

        if (!empty($request->password)) {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return redirect()->route('user.index')->with('success', 'User profile has been successfully updated.');
    }

    public function destroy($id)
    {
        User::destroy($id);

        return redirect()->route('user.index')
            ->with('success', 'User deleted successfully.');
    }
}