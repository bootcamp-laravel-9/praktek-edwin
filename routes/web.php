<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ConsumeApiController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::group(['middleware' => ['guest']], function () {
    Route::get('/login', [AuthController::class, 'loginView'])->name('loginView');
    Route::post('/loginAttempt', [AuthController::class, 'loginAttempt'])->name('loginAttempt');
});

Route::group(['middleware' => ['isSessionValid']], function () {

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/users', [UserController::class, 'index'])->name('user.index');
    Route::get('/users/create', [UserController::class, 'create'])->name('user.create');
    Route::post('/users', [UserController::class, 'store'])->name('users.store');
    Route::delete('/userDelete/{id}', [UserController::class, 'destroy'])->name('users.destroy');
    Route::get('userEdit/{id}', [UserController::class, 'updateUser']);
    Route::put('userEditProcess/{id}', [UserController::class, 'update'])->name('user.update');
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::get('/getTickets', [ConsumeApiController::class, 'index'])->name('ticket.index');
    Route::post('/addTickets', [ConsumeApiController::class, 'create']);
    Route::get('/createTickets', [ConsumeApiController::class, 'showCreateForm'])->name('ticket.create');
});