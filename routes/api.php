<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/getTicket', [TicketController::class, 'index'])->name('index');
Route::post('/addTicket', [TicketController::class, 'create'])->name('create');
Route::put('/updateTicket/{id}', [TicketController::class, 'update'])->name('tickets.update');
Route::delete('/deleteTicket/{id}', [TicketController::class, 'destroy'])->name('tickets.destroy');